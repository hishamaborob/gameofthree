package gameofthree;

public interface Listener {

    void onMessage(String message);
}
